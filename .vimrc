set number
set cindent
set smartindent
syntax enable
set tabstop=4
set expandtab
set shiftwidth=4

set encoding=utf-8

set list
set listchars=tab:>-,trail:·,nbsp:%,extends:>,precedes:<

set noswapfile
set cursorline
set clipboard+=unnamed
autocmd BufNewFile *.html 0r ~/.vim/templates/skel.html
if has('vim_starting')
set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

call neobundle#begin(expand('~/.vim/bundle/'))
NeoBundleFetch 'Shougo/neobundle.vim'
call neobundle#end()

" Let NeoBundle manage NeoBundle
NeoBundleFetch 'Shougo/neobundle.vim'

" add plugins

filetype plugin indent on

NeoBundleCheck
NeoBundle 'tyru/caw.vim.git'
NeoBundle 'tomasr/molokai'
NeoBundle 'tpope/vim-surround'
NeoBundle 'vim-ruby/vim-ruby'
NeoBundle 'thinca/vim-quickrun'
autocmd FileType ruby compiler ruby

set t_Co=256
colorscheme molokai
syntax on
let g:molokai_original = 1

nmap <Leader>c <Plug>(caw:i:toggle)
vmap <Leader>c <Plug>(caw:i:toggle)

let tex_flavor='latex'
set grepprg=grep\ -nH\ $*
set shellslash
let g:Tex_DefaultTargetFormat='pdf'
let g:Tex_CompileRule_dvi='platex --interaction=nonstopmode $*'
let g:Tex_FormatDependency_pdf='dvi,pdf'

" PDFはPreview.appで開く
" let g:Tex_ViewRule_pdf='open -a Preview.spp
